using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StudentManager : MonoBehaviour
{
    [SerializeField] MainManager mainManager;
    [SerializeField] Menu menu;
    [SerializeField] MenuWindow menuWindow;
    int studentToInit = 0;
    int studentWayToGo = 0;
    List<Quaternion> studentsRotations = new List<Quaternion>() { new Quaternion(0, 180, 0, 0), new Quaternion(0, 0, 0, 0) };
    List<Vector3> studentsStartMovement = new List<Vector3>() { new Vector3(100, 0, 0), new Vector3(-100, 0, 0) };
    [SerializeField] List<GameObject> studentsPrefabs;
    List<Student> students = new List<Student>();
    List<int> studentsStartPositionX = new List<int> { -1400, 1400 };
    List<int> studentsStartPositionY = new List<int> { -250, 30 };
    List<Vector3> studentsDirection = new List<Vector3>();
    int studentMaxX = 1400;
    int studentMaxY = 100;
    int studentMinY = -300;
    float chanceToTurn;
    float summaryChanceToTurn = 0.55f;
    float chanceJump = 0.05f;
    void Start()
    {
        InitStudents(1);
    }
    public void AddStudentsDirection(Vector3 studentDirection)
    {
        studentsDirection.Add(studentDirection);
        IncreaceChanceToTurn();
    }
    public void IncreaceChanceToTurn()
    {
        chanceToTurn = summaryChanceToTurn / studentsDirection.Count();
        summaryChanceToTurn += chanceJump;
    }
    public int GetStudentsCount()
    {
        return students.Count;
    }
    public int GetMaxX()
    {
        return studentMaxX;
    }
    public int GetMaxY()
    {
        return studentMaxY;
    }
    public int GetMinY()
    {
        return studentMinY;
    }
    public float GetChanceToTurn()
    {
        return chanceToTurn;
    }
    public int GetPotencialTurnCount()
    {
        return studentsDirection.Count;
    }
    public Vector3 GetPotencialTurn(int index)
    {
        return studentsDirection[index];
    }
    public void InitStudents(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            int studentYPosition = Random.Range(studentsStartPositionY[0], studentsStartPositionY[1]);
            GameObject student = Instantiate(studentsPrefabs[studentToInit], transform);
            student.transform.rotation = studentsRotations[studentWayToGo];
            student.transform.localPosition = new Vector3(studentsStartPositionX[studentWayToGo], studentYPosition, 0);
            students.Add(student.GetComponent<Student>());
            students.Last().InitProperties(mainManager, this, new Vector3(studentsStartPositionX[studentWayToGo], studentYPosition, 0), studentsStartMovement[studentWayToGo], studentsStartMovement[studentWayToGo]);
            studentToInit = (studentToInit + 1) % studentsPrefabs.Count;
            studentWayToGo = (studentWayToGo + 1) % 2;
        }
        StartCoroutine(WaitToSpawnNextStudent(amount));
            menu.UpdateStudentsText(students.Count);
            menuWindow.UpdateStudentsText(students.Count);
    }
    IEnumerator WaitToSpawnNextStudent(int amount)
    {
        for (int i = students.Count-amount; i < students.Count; i++)
        {
            yield return new WaitForSeconds(0.01f);
            students[i].ChangeCanMove();
        }
    }
}
