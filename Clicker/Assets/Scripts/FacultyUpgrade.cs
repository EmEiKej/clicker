using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FacultyUpgrade : MonoBehaviour
{
    [SerializeField] TMP_FontAsset textFont;
    float upgradeValue = 1.5f;
    Faculty faculty;
    Color colorRed = new Color32(140, 80, 80, 255);
    MainManager mainManager;
    [SerializeField] TMP_Text facultyNameText;
    [SerializeField] TMP_Text levelText;
    [SerializeField] TMP_Text moneyPerClickText;
    [SerializeField] TMP_Text moneyPerClickBonusText;
    [SerializeField] TMP_Text studentsAmountText;
    [SerializeField] TMP_Text studentsAmountBonusText;
    [SerializeField] TMP_Text priceText;
    [SerializeField] Button priceButton;
    [SerializeField] GameObject CoinIcon;
    [SerializeField] GameObject LockIcon;
    [SerializeField] GameObject PlusIcon;
    [SerializeField] GameObject PlusIcon1;
    string facultyName;
    int level;
    int moneyPerClick;
    int moneyPerClickBonus;
    int studentsAmount;
    int studentsAmountBonus;
    int price;
    public string GetFacultyName() 
    {
        return facultyName;
    }
    public void UpdateButtonColor(float money)
    {
        if (level < 6)
        {
            if (money >= price)
            {
                priceButton.GetComponent<SpriteRenderer>().color = Color.white;
            }
            else
            {
                priceButton.GetComponent<SpriteRenderer>().color = colorRed;
            }
        }
    }
    public void SetData(Faculty faculty, MainManager mainManager)
    {
        this.faculty = faculty;
        this.mainManager = mainManager;
        facultyName = faculty.GetFacultyName();
        facultyNameText.text = facultyName;
        moneyPerClick = faculty.moneyPerClick;
        moneyPerClickText.text = moneyPerClick.ToString();
        studentsAmount = faculty.studentsAmount;
        studentsAmountText.text = studentsAmount.ToString();
        level = faculty.GetLevel() + 1;
        if (level == 6)
        {
            levelText.text = "5";
            priceButton.GetComponent<SpriteRenderer>().color = Color.yellow;
            priceButton.interactable = false;
            priceText.text = "Max";
            priceText.font = textFont;
            CoinIcon.SetActive(false);
            LockIcon.SetActive(false);
            PlusIcon.SetActive(false);
            PlusIcon1.SetActive(false);
            moneyPerClickBonusText.text = string.Empty;
            studentsAmountBonusText.text = string.Empty;
        }
        else
        {
            levelText.text = level.ToString();
            moneyPerClickBonus = Mathf.CeilToInt(faculty.moneyPerClick * upgradeValue) - moneyPerClick;
            moneyPerClickBonusText.text = moneyPerClickBonus.ToString();
            studentsAmountBonus = Mathf.CeilToInt(faculty.studentsAmount * upgradeValue) - studentsAmount;
            studentsAmountBonusText.text = studentsAmountBonus.ToString();
            price = Mathf.CeilToInt(faculty.price * upgradeValue);
            priceText.text = price.ToString();
            UpdateButtonColor(mainManager.GetMoney());

        }

    }
    public void OnPriceButtonClicked()
    {
        if (priceButton.GetComponent<SpriteRenderer>().color == Color.white)
        {
            mainManager.UpdateMoney(price);
            mainManager.studentManager.InitStudents(studentsAmountBonus);
            mainManager.studentManager.IncreaceChanceToTurn();
            studentsAmount += studentsAmountBonus;
            studentsAmountText.text = studentsAmount.ToString();
            moneyPerClick += moneyPerClickBonus;
            moneyPerClickText.text = moneyPerClick.ToString();
            faculty.SetLevel(level);
            faculty.moneyPerClick = moneyPerClick;
            faculty.studentsAmount = studentsAmount;
            faculty.price = price;
            level += 1;
            if (level == 6)
            {
                priceButton.GetComponent<SpriteRenderer>().color = Color.yellow;
                priceButton.interactable = false;
                priceText.text = "Max";
                priceText.font = textFont;
                CoinIcon.SetActive(false);
                LockIcon.SetActive(false);
                PlusIcon.SetActive(false);
                PlusIcon1.SetActive(false);
                moneyPerClickBonusText.text = string.Empty;
                studentsAmountBonusText.text = string.Empty;
                faculty.ChangeSprite();
            }
            else
            {
                levelText.text = level.ToString();
                moneyPerClickBonus = Mathf.CeilToInt(moneyPerClick * upgradeValue) - moneyPerClick;
                moneyPerClickBonusText.text = moneyPerClickBonus.ToString();
                studentsAmountBonus = Mathf.CeilToInt(studentsAmount * upgradeValue) - studentsAmount;
                studentsAmountBonusText.text = studentsAmountBonus.ToString();
                price = Mathf.CeilToInt(price * upgradeValue);
                priceText.text = price.ToString();
                UpdateButtonColor(mainManager.GetMoney());
            }
        }
    }
}
