using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FacultyWindow : MonoBehaviour
{
    Color colorRed = new Color32(140, 80, 80, 255);
    Faculty faculty;
    [SerializeField] MainManager mainManager;
    [SerializeField] StudentManager studentManager;
    [SerializeField] TMP_Text facultyNameText;
    [SerializeField] TMP_Text moneyPerClickText;
    [SerializeField] TMP_Text studentsAmountText;
    [SerializeField] TMP_Text priceText;
    [SerializeField] Button priceButton;
    public void UpdateButtonColor(float money)
    {
        if (gameObject.activeSelf)
        {
            if (money >= faculty.price)
            {
                priceButton.GetComponent<SpriteRenderer>().color = Color.white;
            }
            else
            {
                priceButton.GetComponent<SpriteRenderer>().color = colorRed;
            }
        }
    }
    public void OnButtonExitClick()
    {
        gameObject.SetActive(false);
        mainManager.SwitchButtons(true);
    }
    public void ShowDate(string facultyName, int price, int studentsAmount, int moneyPerClick, double money, Faculty faculty)
    {
        this.faculty = faculty;
        facultyNameText.text = facultyName;
        priceText.text = price.ToString();
        studentsAmountText.text = studentsAmount.ToString();
        moneyPerClickText.text = moneyPerClick.ToString();
        if (money >= price)
        {
            priceButton.GetComponent<SpriteRenderer>().color = Color.white;
        }
        else
        {
            priceButton.GetComponent<SpriteRenderer>().color = colorRed;
        }
    }
    public void OnButtonPriceClicked()
    {
        if (priceButton.GetComponent<SpriteRenderer>().color == Color.white)
        {
            mainManager.UpdateMoney(faculty.price);
            gameObject.SetActive(false);
            faculty.TunrOffPriceGameObject();
            faculty.ChangeIsBought();
            studentManager.InitStudents(faculty.studentsAmount);
            studentManager.AddStudentsDirection(faculty.transform.localPosition);
            mainManager.SwitchButtons(true);
        }
    }
}
