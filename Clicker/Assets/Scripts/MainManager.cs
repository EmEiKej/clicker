using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{
    byte encryptionKey = 42;
    string filePath;
    [SerializeField] public StudentManager studentManager;
    [SerializeField] Menu menu;
    [SerializeField] MenuWindow menuWindow;
    [SerializeField] FacultyWindow facultyWindow;
    [SerializeField] public List<Faculty> faculties;
    [SerializeField] public GameObject facultiesFolder;
    float money = 0;
    float multiplier = 1;
    float multiplierJump = 0.005f;
    double multiplierLimit = 10;
    int timeLimit = 3;
    List<string> facultiesName = new List<string>() { "Faculty of Philology", "Faculty of Chemistry", "Faculty of Engineering", "Faculty of Economics", "Faculty of Physics", "Faculty of Law", "Faculty of Architecture", "Faculty of Computer Science", "Faculty of Medicine" };
    List<int> facultiesPrice = new List<int>() { 1000, 5000, 10000, 50000, 75000, 100000, 500000, 750000, 1000000 };
    List<int> faculitesMoneyPerClick = new List<int>() { 1, 2, 5, 10, 20, 40, 80, 120, 150 };
    List<int> facultiesNumerOfStudents = new List<int>() { 1, 2, 3, 4, 5, 7, 9, 12, 16 };
    Stopwatch timer = new Stopwatch();
    void Start()
    {
        filePath = Path.Combine(Application.persistentDataPath, "data.txt");
        for (int i = 0; i < faculties.Count; i++)
        {
            faculties[i].SetValues(i, faculitesMoneyPerClick[i], facultiesName[i], facultiesPrice[i], i == 0, facultiesNumerOfStudents[i]);
        }
        studentManager.AddStudentsDirection(faculties[0].transform.localPosition);
        studentManager.AddStudentsDirection(menu.gameObject.transform.localPosition);
        ReadData();
        UpdateMoneyObjects();
    }
    void Update()
    {
        if (timer.Elapsed.TotalSeconds >= timeLimit)
        {

            multiplier = 1;
            menu.UpdateMultiplaierText(multiplier);
            timer.Reset();
        }
    }
    private void OnApplicationQuit()
    {
        List<int> isBought = new List<int>();
        for (int i = 1; i < faculties.Count; i++)
        {
            isBought.Add(faculties[i].GetIsBought() ? 1 : 0);
        }
        List<int> upgradeBuild = new List<int>();
        for (int i = 0; i < faculties.Count; i++)
        {
            upgradeBuild.Add(faculties[i].GetLevel());
        }
        SaveData(money, studentManager.GetStudentsCount(), isBought, upgradeBuild);
        Application.Quit();
    }
    private void OnApplicationPause()
    {
        List<int> isBought = new List<int>();
        for (int i = 1; i < faculties.Count; i++)
        {
            isBought.Add(faculties[i].GetIsBought() ? 1 : 0);
        }
        List<int> upgradeBuild = new List<int>();
        for (int i = 0; i < faculties.Count; i++)
        {
            upgradeBuild.Add(faculties[i].GetLevel());
        }
        SaveData(money, studentManager.GetStudentsCount(), isBought, upgradeBuild);
    }
    public void SwitchButtons(bool state)
    {
        foreach (Faculty faculty in faculties)
        {
            faculty.SwitchPriceButton(state);
        }
        menu.SwitchMenuButton(state);
    }
    void UpdateMoneyObjects()
    {
        menu.UpdateMoneyText(money);
        menuWindow.UpdateMoneyText(money);
        facultyWindow.UpdateButtonColor(money);
    }
    public void UpdateMoney(int amount)
    {
        money -= amount;
        UpdateMoneyObjects();
    }
    public float GetMoney()
    {
        return money;
    }
    public void OpenFacultyWindow(string facultyName, int price, int studentsAmount, int moneyPerClick, Faculty faculty)
    {
        facultyWindow.gameObject.SetActive(true);
        facultyWindow.ShowDate(facultyName, price, studentsAmount, moneyPerClick, money, faculty);
        SwitchButtons(false);
    }
    public void AddMoney(int amount)
    {
        money += multiplier * amount;
        UpdateMoneyObjects();
    }
    public void OnFacultyButtonClickedAction(float moneyPerClick)
    {
        AddMoney((int)moneyPerClick);
        if (multiplier <= multiplierLimit - multiplierJump)
        {
            multiplier += multiplierJump;
            menu.UpdateMultiplaierText(multiplier);

        }
        if (timer.IsRunning)
        {
            timer.Restart();
        }
        else
        {
            timer.Start();
        }
    }
    public void SaveData(float money, int students, List<int> boughtBuild, List<int> upgradeBuild)
    {
        string content = money.ToString() + ";" + students.ToString() + ";" + string.Join(";", boughtBuild) + ";" + string.Join(";", upgradeBuild);
        EncryptData(content, encryptionKey, filePath);
        void EncryptData(string inputData, byte encryptionKey, string filePath)
        {
            try
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(inputData);
                byte[] encryptedBytes = new byte[inputBytes.Length];
                for (int i = 0; i < inputBytes.Length; i++)
                {
                    encryptedBytes[i] = (byte)(inputBytes[i] ^ encryptionKey);
                }
                string encryptedBase64 = Convert.ToBase64String(encryptedBytes);
                File.WriteAllText(filePath, encryptedBase64);
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log($"Error while encrypting and saving the string: {ex.Message}");
            }
        }
    }
    public void ReadData()
    {
        var values = GetData();
        if (values.Item3.Count != 0 && values.Item4.Count != 0)
        {
            money = values.Item1;
            studentManager.InitStudents(values.Item2 - 1);
            faculties[0].SetLevel(values.Item4[0]);
            for (int i = 0; i < faculties[0].GetLevel(); i++)
            {
                studentManager.IncreaceChanceToTurn();
                faculties[0].moneyPerClick += Mathf.CeilToInt(faculties[0].moneyPerClick * 0.5f);
                faculties[0].studentsAmount += Mathf.CeilToInt(faculties[0].studentsAmount * 0.5f);
                faculties[0].price += Mathf.CeilToInt(faculties[0].price * 0.5f);
            }
            if (faculties[0].GetLevel() == 5)
            {
                faculties[0].ChangeSprite();
            }
            for (int i = 1; i < faculties.Count; i++)
            {
                if (values.Item3[i - 1] == 1)
                {
                    faculties[i].TunrOffPriceGameObject();
                    faculties[i].ChangeIsBought();
                    studentManager.AddStudentsDirection(faculties[i].transform.localPosition);
                    faculties[i].SetLevel(values.Item4[i]);
                    if (values.Item4[i] != 0)
                    {
                        for (int j = 0; j < faculties[i].GetLevel(); j++)
                        {
                            studentManager.IncreaceChanceToTurn();
                            faculties[i].moneyPerClick += Mathf.CeilToInt(faculties[i].moneyPerClick * 0.5f);
                            faculties[i].studentsAmount += Mathf.CeilToInt(faculties[i].studentsAmount * 0.5f);
                            faculties[i].price += Mathf.CeilToInt(faculties[i].price * 0.5f);
                        }
                    }
                    if (faculties[i].GetLevel() == 5)
                    {
                        faculties[i].ChangeSprite();
                    }
                }
            }
        }
        (float, int, List<int>, List<int>) GetData()
        {
            string DecryptData(string filePath, byte encryptionKey)
            {
                if (!File.Exists(filePath))
                {
                    return string.Empty;
                }
                try
                {
                    string encryptedBase64 = File.ReadAllText(filePath);
                    byte[] encryptedBytes = Convert.FromBase64String(encryptedBase64);
                    byte[] decryptedBytes = new byte[encryptedBytes.Length];
                    for (int i = 0; i < encryptedBytes.Length; i++)
                    {
                        decryptedBytes[i] = (byte)(encryptedBytes[i] ^ encryptionKey);
                    }
                    string decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                    return decryptedData;
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.Log($"Error while decrypting from file: {ex.Message}");
                    return null;
                }
            }
            string content = DecryptData(filePath, encryptionKey);
            if (content != string.Empty)
            {
                List<string> values = content.Split(';').ToList();
                float money = float.Parse(values[0]);
                int students = int.Parse(values[1]);
                List<int> boughtBuild = values.GetRange(2, 8).Select(int.Parse).ToList();
                List<int> upgradeBuild = values.GetRange(10, 9).Select(int.Parse).ToList();
                return (money, students, boughtBuild, upgradeBuild);
            }
            else
            {
                return (0, 0, new List<int>(), new List<int>());
            }
        }
    }
}
