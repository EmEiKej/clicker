using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Student : MonoBehaviour
{
    MainManager mainManager;
    StudentManager studentManager;
    int moneyProfit = 10;
    bool movesHorizontal = true;
    bool triedToTurn = false;
    bool canMove = false;
    Vector3 studentStartPosition;
    Vector3 studentStartMovement;
    Vector3 movement;
    public void ChangeCanMove() 
    {
        canMove = true;
    }
    void Update()
    {
        if (!canMove)
        {
            return;
        }
        if (transform.localPosition.x < -studentManager.GetMaxX() || transform.localPosition.x > studentManager.GetMaxX())
        {
            transform.localPosition = studentStartPosition;
            movement = studentStartMovement;
            movesHorizontal = true;
        }
        else if (transform.localPosition.y < studentManager.GetMinY() || transform.localPosition.y > studentManager.GetMaxY())
        {
            transform.localPosition = studentStartPosition;
            movement = studentStartMovement;
            movesHorizontal = true;
            mainManager.AddMoney(moneyProfit);
        }
        else
        {
            if (movesHorizontal)
            {
                if (!triedToTurn)
                {
                    for (int i = 0; i < studentManager.GetPotencialTurnCount(); i++)
                    {
                        Vector3 potencialTurn = studentManager.GetPotencialTurn(i);
                        if ((int)transform.localPosition.x > (int)potencialTurn.x-5&& (int)transform.localPosition.x < (int)potencialTurn.x+5)
                        {
                            triedToTurn = true;
                            float random = Random.Range(0f, 1f);
                            if (random < studentManager.GetChanceToTurn())
                            {
                                if (potencialTurn.y > 0)
                                {
                                    movement = new Vector3(0, 100, 0);
                                }
                                else
                                {
                                    movement = new Vector3(0, -100, 0);
                                }
                                movesHorizontal = false;
                                return;
                            }
                        }
                    }
                }
                else
                {
                    StartCoroutine(WaitToChangeTriedToTurn());
                }
            }
        }
        transform.localPosition += movement * Time.deltaTime;
    }
    IEnumerator WaitToChangeTriedToTurn()
    {
        yield return new WaitForSeconds(0.5f);
        triedToTurn = false;
    }
    public void InitProperties(MainManager mainManager, StudentManager studentManager, Vector3 studentStartPosition, Vector3 studentStartMovement, Vector3 movement)
    {
        this.mainManager = mainManager;
        this.studentManager = studentManager;
        this.studentStartPosition = studentStartPosition;
        this.studentStartMovement = studentStartMovement;
        this.movement = movement;
    }
}
