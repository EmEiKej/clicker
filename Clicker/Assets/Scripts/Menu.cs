using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] MainManager mainManager;
    [SerializeField] MenuWindow menuWindow;
    [SerializeField] TMP_Text moneyText;
    [SerializeField] TMP_Text studentsText;
    [SerializeField] TMP_Text multiplaierText;
    [SerializeField] Button menuButton;
    public void SwitchMenuButton(bool state)
    {
        menuButton.interactable = state;
    }
    public void UpdateMoneyText(float money)
    {
        moneyText.text = money.ToString("0");
    }
    public void UpdateStudentsText(int students)
    {
        studentsText.text = students.ToString();
    }
    public void UpdateMultiplaierText(float multiplaier)
    {
        multiplaierText.text = multiplaier.ToString("0.00");
    }
    public void OnButtonMenuClicked()
    {
        gameObject.SetActive(false);
        mainManager.facultiesFolder.SetActive(false);
        menuWindow.gameObject.SetActive(true);
        menuWindow.OnButtonMenuClickedAction();
    }
}
