using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class MenuWindow : MonoBehaviour
{
    [SerializeField] MainManager mainManager;
    [SerializeField] StudentManager studentManager;
    [SerializeField] Menu menu;
    [SerializeField] TMP_Text moneyText;
    [SerializeField] TMP_Text studentsText;
    [SerializeField] GameObject facultyUpgradePrefab;
    [SerializeField] Transform scrollViewContentTransform;
    List<FacultyUpgrade> facultiesUpgrade = new List<FacultyUpgrade>();
    public void OnButtonMenuClickedAction()
    {
        foreach (Faculty faculty in mainManager.faculties)
        {
            if (faculty.GetIsBought())
            {
                bool canInit = true;
                foreach (FacultyUpgrade facultyUpgrade in facultiesUpgrade)
                {
                    if (facultyUpgrade.GetFacultyName() == faculty.GetFacultyName())
                    {
                        canInit = false;
                    }
                }
                if (canInit)
                {
                    facultiesUpgrade.Add(Instantiate(facultyUpgradePrefab, scrollViewContentTransform).GetComponent<FacultyUpgrade>());
                    facultiesUpgrade.Last().SetData(faculty, mainManager);
                }
            }
        }
    }
    public void UpdateMoneyText(float money)
    {
        moneyText.text = money.ToString("0");
        foreach (FacultyUpgrade facultyUpgrade in facultiesUpgrade)
        {
            facultyUpgrade.UpdateButtonColor(money);
        }
    }
    public void UpdateStudentsText(int students)
    {
        studentsText.text = students.ToString();
    }
    public void OnButtonExitClicked()
    {
        menu.gameObject.SetActive(true);
        mainManager.facultiesFolder.SetActive(true);
        gameObject.SetActive(false);
    }
}
