using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Faculty : MonoBehaviour
{
    [SerializeField] Sprite facultyFrontMax;
    [SerializeField] Sprite facultyBackMax;
    int id;
    int level = 0;
    public int moneyPerClick;
    string facultyName;
    public int price;
    bool isBought;
    public int studentsAmount;
    [SerializeField] MainManager mainManager;
    [SerializeField] TMP_Text facultyNameText;
    [SerializeField] TMP_Text priceText;
    [SerializeField] GameObject priceGameObject;
    [SerializeField] Button priceButton;
    public string GetFacultyName() 
    {
        return facultyName;
    }
    public bool GetIsBought()
    {
        return isBought;
    }
    public int GetLevel() 
    {
        return level;
    }
    public void SetLevel(int level) 
    {
        this.level = level;
    }
    public void TunrOffPriceGameObject() 
    {
        priceGameObject.SetActive(false);
    }
    public void SwitchPriceButton(bool state) 
    {
        priceButton.interactable = state;
    }
    public void On_Faculty_ButtonClicked()
    {
        if (isBought)
        {
            mainManager.OnFacultyButtonClickedAction(moneyPerClick);
        }
        else
        {
           mainManager.OpenFacultyWindow(facultyName, price, studentsAmount, moneyPerClick, this);
        }
    }
    public void ChangeSprite() 
    {
        if (id < 5)
        {
            priceButton.GetComponent<SpriteRenderer>().sprite = facultyFrontMax;   
        }
        else 
        {
            priceButton.GetComponent<SpriteRenderer>().sprite = facultyBackMax;
        }
    }
    public void SetValues(int id,int moneyPerClick, string facultyName, int price, bool isBought,int studentsAmount)
    {
        this.id = id;
        this.moneyPerClick = moneyPerClick;
        this.facultyName = facultyName;
        this.price = price;
        this.isBought = isBought;
        this.studentsAmount = studentsAmount;
        facultyNameText.text = facultyName;
        priceText.text = price.ToString();
    }
    public void ChangeIsBought()
    {
        isBought = true;
    }
}
